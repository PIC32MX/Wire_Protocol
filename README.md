## Graphics Terminal Serial Protocol

This project contains the XML source and images for a document
desscribing the serial protocol used by the Serial Graphics Terminal
(https://gitlab.com/PIC32MX/graphicsTerminal).

The XML is intended to be processed by Publican, and relies on a
Publican brand which is not yet on GitLab but is at:
http://71.10.153.139:3665/git/gitweb.cgi?p=Publican_brand/elmer166.git;a=summary


