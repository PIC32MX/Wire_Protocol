<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % BOOK_ENTITIES SYSTEM "Wire_Protocol.ent">
%BOOK_ENTITIES;
]>
<section id="sect-graph">
  <title>Graph Commands</title>
  <para>
    This series of commands are related and need to be used together.
    A minimum sequence might be:
    <simplelist>
     <member><command>TFTGINIT</command> - reset graph parameters to
     defaults</member> 
     <member><command>TFTGEXPOSE</command> - render the graph frame</member>
     <member><command>TFTGADDPT</command> - repeated for each point</member>
    </simplelist>
  </para>
  <para>
    A more complete list of operations might be:
    <simplelist>
     <member><command>TFTGINIT</command> - reset graph parameters to
     defaults</member>
     <member><command>TFTGCOLORS</command> - set the colors for the graph</member>
     <member><command>TFTGRANGE</command> - set the ranges for the axes</member>
     <member><command>TFTGTITLE</command> - add a title to the graph</member>
     <member><command>TFTGXLABEL</command> - label the X axis</member>
     <member><command>TFTGYLABEL</command> - label the Y axis</member>
     <member><command>TFTGEXPOSE</command> - render the graph frame</member>
     <member><command>TFTGADDPT</command> - repeated for each point</member>
    </simplelist>
    <command>TFTGCOLORS</command>, <command>TFTGRANGE</command>,
    <command>TFTGTITLE</command>, <command>TFTGXLABEL</command>, and
    <command>TFTGYLABEL</command> must be sent after
    <command>TFTGINIT</command> and before
    <command>TFTGEXPOSE</command> but otherwise the order doesn't
    matter.  If <command>TFTGADDPT</command> is sent before
    <command>TFTGEXPOSE</command>, <command>TFTGEXPOSE</command> will
    erase any points drawn.
  </para>
  <para>
    Note that the caller might simply dispense with all the cosmetics
    and begin calling <command>TFTGADDPT</command> multiple times.
    The line would still be drawn but there would be no decoration
    beyond what the caller may have provided with other calls.
    However, without having called <command>TFTGINIT</command> the
    colors might be random, with a black line on a black background
    highly probable. <command>TFTGINIT</command> provides a reasonable
    set of default colors.
  </para>

    <section id="sect-tftginit">
      <title>Initialize the graph</title>
      <para>
	<indexterm><primary>TFTGINIT</primary></indexterm>
	<indexterm><primary>Initialization</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Initialization</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x19</secondary>
	</indexterm>
	Initialize the graph. Resets all graph parameters to their
	defaults. 
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGINIT</command>
	</cmdsynopsis>
      </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGINIT</command> command has a value of 0x19.
      </para>
    </section>


    <section id="sect-tftgaddpt">
      <title>Add a point to the graph</title>
      <para>
	<indexterm><primary>TFTGADDPT</primary></indexterm>
	<indexterm><primary>Add point</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Add point</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x1a</secondary>
	</indexterm>
	Add a point to a graph which has already been exposed.
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGADDPT</command>
	  <arg choice="req"><replaceable>y</replaceable></arg>
	</cmdsynopsis>
 	<itemizedlist spacing="compact">
	  <listitem>
	    <para>
	      <command>y</command> - The desired value to be
	      addded to the graph.  Caller is responsible for
	      scaling.
	    </para>
	  </listitem>
	</itemizedlist>
     </para>
     <para>
       <command>TFTGADDPT</command> is the simplest of the graphing
       commands.  The X value is set to zero when
       <command>TFTGINIT</command> is called, and incremented each
       time a new point is provided via
       <command>TFTGADDPT</command>.  When the graph extends past the
       right edge of the screen, X is reset to zero.  The caller
       provides the vertical address of the new point, 0..190.  Values
       outside that range will be limited to that range.
     </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGADDPT</command> command has a value of 0x1a.
      </para>
    </section>


    <section id="sect-tftgexpose">
      <title>Render the graph frame and annotation</title>
      <para>
	<indexterm><primary>TFTGEXPOSE</primary></indexterm>
	<indexterm><primary>Render graph</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Expose</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x1b</secondary>
	</indexterm>
	Expose the graph, clears the screen, draws the graph frame and
	any requested annotation.
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGEXPOSE</command>
	</cmdsynopsis>
      </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGEXPOSE</command> command has a value of 0x1b.
      </para>
    </section>


    <section id="sect-tftgcolors">
      <title>Specify colors to be used for the graph</title>
      <para>
	<indexterm><primary>TFTGCOLORS</primary></indexterm>
	<indexterm><primary>Colors for graph</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Colors</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x1c</secondary>
	</indexterm>
	Specify each of the colors to be used for the graph
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGCOLORS</command>
	  <arg choice="req"><replaceable>title</replaceable></arg>
	  <arg choice="req"><replaceable>annotation</replaceable></arg>
	  <arg choice="req"><replaceable>xlabel</replaceable></arg>
	  <arg choice="req"><replaceable>ylabel</replaceable></arg>
	  <arg choice="req"><replaceable>screen</replaceable></arg>
	  <arg choice="req"><replaceable>field</replaceable></arg>
	  <arg choice="req"><replaceable>data</replaceable></arg>
	</cmdsynopsis>
 	<itemizedlist spacing="compact">
	  <listitem>
	    <para>
	      <command>title</command> - The 16-bit color to be used
	      for the title of the graph.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>annotation</command> - The 16-bit color to be used
	      for the axis annotation of the graph.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>xlabel</command> - The 16-bit color to be used
	      for the X-axis label of the graph.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>ylabel</command> - The 16-bit color to be used
	      for the Y-axis label of the graph.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>screen</command> - The 16-bit color to be used
	      for the outside of the graph.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>field</command> - The 16-bit color to be used
	      for the plotting area of the graph.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>data</command> - The 16-bit color to be used
	      for the graph data.
	    </para>
	  </listitem>
	</itemizedlist>
     </para>
     <para>
       If <command>TFTGCOLORS</command> is not provided, default
       colors will be used.
        <figure float="0">
          <title>Default Colors</title>
          <mediaobject>
            <imageobject><imagedata scale="99" scalefit="1" 
            fileref="images/GraphDefaultColors.png" 
	      format="PNG"/></imageobject>
            <textobject><para>
              Default graph colors
            </para>
            </textobject>
          </mediaobject>
        </figure>
      </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGCOLORS</command> command has a value of 0x1c.
      </para>
    </section>


    <section id="sect-tftgrange">
      <title>Specify values placed on the axes</title>
      <para>
	<indexterm><primary>TFTGRANGE</primary></indexterm>
	<indexterm><primary>Limits for graph</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Range</secondary>
	</indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Axes</secondary>
	</indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>X range</secondary>
	</indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Y range</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x1d</secondary>
	</indexterm>
	Specify the limits of the graph axes
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGRANGE</command>
	  <arg choice="req"><replaceable>Xmin</replaceable></arg>
	  <arg choice="req"><replaceable>Xmax</replaceable></arg>
	  <arg choice="req"><replaceable>Ymin</replaceable></arg>
	  <arg choice="req"><replaceable>Ymax</replaceable></arg>
	</cmdsynopsis>
 	<itemizedlist spacing="compact">
	  <listitem>
	    <para>
	      <command>Xmin</command> - The minimum value for the
	      graph's X-axis
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>Xmax</command> - The maximum value for the
	      graph's X-axis
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>Ymin</command> - The minimum value for the
	      graph's Y-axis
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>Ymax</command> - The maximum value for the
	      graph's Y-axis
	    </para>
	  </listitem>
	</itemizedlist>
     </para>
     <para>
       If <command>TFTGRANGE</command> is not provided, the axes will
       not be annotated.  If zeros are provided for both the minimum
       and maximum for either the X or Y axis, that axis will not be
       annotated. 
     </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGRANGE</command> command has a value of 0x1d.
      </para>
    </section>


    <section id="sect-tftgtitle">
      <title>Specify a title for the graph</title>
      <para>
	<indexterm><primary>TFTGTITLE</primary></indexterm>
	<indexterm><primary>Title for graph</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Title</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x1e</secondary>
	</indexterm>
	Specify a title for the graph
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGTITLE</command>
	  <arg choice="plain" rep="repeat"><replaceable>title</replaceable></arg>
	</cmdsynopsis>
 	<itemizedlist spacing="compact">
	  <listitem>
	    <para>
	      <command>title</command> - Twenty-four ASCII characters
	      including one or more terminating NULL characters.
	      Titles shorter than 23 characters must be padded with
	      trailing NULL characters.
	    </para>
	  </listitem>
	</itemizedlist>
     </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGTITLE</command> command has a value of 0x1e.
      </para>
    </section>


    <section id="sect-tftgxlabel">
      <title>Specify a label for the graph's X axis</title>
      <para>
	<indexterm><primary>TFTGXLABEL</primary></indexterm>
	<indexterm><primary>X axis label for graph</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>X-Label</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x1f</secondary>
	</indexterm>
	Specify a label for the graph's X-axis
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGXLABEL</command>
	  <arg choice="plain" rep="repeat"><replaceable>label</replaceable></arg>
	</cmdsynopsis>
 	<itemizedlist spacing="compact">
	  <listitem>
	    <para>
	      <command>label</command> - Twenty-four ASCII characters
	      including one or more terminating NULL characters.
	      Labels shorter than 23 characters must be padded with
	      trailing NULL characters.
	    </para>
	  </listitem>
	</itemizedlist>
     </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGXLABEL</command> command has a value of 0x1f.
      </para>
    </section>


    <section id="sect-tftgylabel">
      <title>Specify a label for the graph's Y axis</title>
      <para>
	<indexterm><primary>TFTGYLABEL</primary></indexterm>
	<indexterm><primary>Y axis label for graph</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Y-Label</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x20</secondary>
	</indexterm>
	Specify a label for the graph's Y-axis
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGYLABEL</command>
	  <arg choice="plain" rep="repeat"><replaceable>label</replaceable></arg>
	</cmdsynopsis>
 	<itemizedlist spacing="compact">
	  <listitem>
	    <para>
	      <command>label</command> - Twenty-four ASCII characters
	      including one or more terminating NULL characters.
	      Labels shorter than 23 characters must be padded with
	      trailing NULL characters.
	    </para>
	  </listitem>
	</itemizedlist>
     </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGYLABEL</command> command has a value of 0x20.
      </para>
    </section>


    <section id="sect-tftgnocursor">
      <title>Graph cursor control</title>
      <para>
	<indexterm><primary>TFTGNOCURSOR</primary></indexterm>
	<indexterm><primary>Cursor</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Cursor</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x21</secondary>
	</indexterm>
	Normally, a cursor is displayed at the next column of dots to
	be displayed by <command>TFTGADDPT</command>.  Sending
	<command>TFTGNOCURSOR</command> dispenses with the display of
	the cursor.
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGNOCURSOR</command>
	</cmdsynopsis>
      </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGNOCURSOR</command> command has a value of 0x21.
      </para>
    </section>


    <section id="sect-tftgnoerase">
      <title>Graph erase control</title>
      <para>
	<indexterm><primary>TFTGNOERASE</primary></indexterm>
	<indexterm><primary>Erase</primary></indexterm>
	<indexterm>
          <primary>Graph</primary>
	  <secondary>Erase</secondary>
	</indexterm>
	<indexterm>
          <primary>Code</primary>
	  <secondary>0x22</secondary>
	</indexterm>
	When a graph wraps around horizontally, the row of dots to
	ther right is erased before displaying the new dot.
	<command>TFTGNOERASE</command> eliminates this behavior,
	allowing all previous traces to be displayed.
	<command>TFTGNOERASE</command> implies
	<command>TFTGNOCURSOR</command>. 
      </para>
      <para>
	<cmdsynopsis>
	  <command>TFTGNOERASE</command>
	</cmdsynopsis>
      </para>
      <para>
	If not using the <filename>&MAINHEADER;</filename> header file, the
	<command>TFTGNOERASE</command> command has a value of 0x22.
      </para>
    </section>

</section>
